"""empty message

Revision ID: 19ef3342c62f
Revises: f5cc09820810
Create Date: 2016-09-15 21:52:51.130406

"""

# revision identifiers, used by Alembic.
revision = '19ef3342c62f'
down_revision = 'f5cc09820810'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('hooks', sa.Column('url_params', sa.TEXT, nullable=True))
    op.add_column('hooks', sa.Column('headers', sa.TEXT, nullable=True))


def downgrade():
    op.drop_column('hooks', 'url_params')
    op.drop_column('hooks', 'headers')
