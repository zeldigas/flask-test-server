"""empty message

Revision ID: 9aac188737ff
Revises: None
Create Date: 2016-09-03 12:56:36.495034

"""

# revision identifiers, used by Alembic.
revision = '9aac188737ff'
down_revision = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        'boxes',
        sa.Column('id', sa.String, primary_key=True),
        sa.Column('expiration_time', sa.DateTime),
        sa.Column('created_at', sa.TIMESTAMP),
        sa.Column('updated_at', sa.TIMESTAMP)
    )
    op.create_table(
        'hooks',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('content', sa.TEXT),
        sa.Column('created_at', sa.TIMESTAMP),
        sa.Column('updated_at', sa.TIMESTAMP),
        sa.Column('box_id', sa.String)
    )


def downgrade():
    op.drop_table('boxes')
    op.drop_table('hooks')
