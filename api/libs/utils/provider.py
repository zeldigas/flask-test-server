import json
import os
from functools import wraps

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from api.libs.utils.tools import Root


class App(object):

    app = None

    @classmethod
    def get_app(cls, *args, **kwargs):
        cls.app = cls.app if cls.app else Flask('flask-test-server', **kwargs)
        return cls.app

    @classmethod
    def load_conf(cls):
        path = os.path.join(Root().path, 'conf')
        for file in [n for n in os.listdir(path) if n.endswith('.json')]:
            file = os.path.join(path, file)
            with open(file, 'r') as f:
                content = json.load(f)
                env = 'DEVELOPMENT'
                for k, v in content[env].items():
                    if cls.app is None:
                        raise AssertionError('Cannot load config into a not existing api.')
                    cls.app.config[k] = v


class Alchemy(object):

    db = None

    @classmethod
    def get_db(cls, app):
        cls.db = cls.db if cls.db else SQLAlchemy(app)
        return cls.db

    @staticmethod
    def auto_commit(func):
        @wraps(func)
        def decorator(*args):
            res = func(*args)
            Alchemy.db.session.commit()
            return res

        return decorator()

