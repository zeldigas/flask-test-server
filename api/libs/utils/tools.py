
import os
from functools import wraps


class Root(object):

    __path = None

    @property
    def path(self):
        type(self).__path = (
            self.__path or
            os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..'))
        )
        return self.__path


def set_content(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        res = f(*args, **kwargs)
        res.headers['Content-Type'] = 'application/vnd.siren+json'
        return res
    return wrapper





