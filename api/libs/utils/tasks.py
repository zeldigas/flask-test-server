import random
import time
from datetime import datetime, timedelta

from api.libs.utils.provider import Alchemy, App


class Task(object):

    @staticmethod
    def db_clean(frequency_per_hour=10):
        from api.app.models.box import Box
        from api.app.models.monitor_log import MonitorLog

        t = 3600 / frequency_per_hour
        time.sleep(random.randrange(int(t)))
        while True:
            time.sleep(t)
            log = MonitorLog.query.order_by(MonitorLog.created_at.desc())
            if not log.all() or (log.first().created_at + timedelta(seconds=t) <= datetime.now()):
                Alchemy.db.session.add(MonitorLog())
                boxes = (Box.query.filter(Box.expiration_time <= datetime.now())
                         .limit(App.get_app().config['MONITOR_LIMIT'])
                         .all())
                for box in boxes:
                    Alchemy.db.session.delete(box)
                for l in log.offset(1).limit(App.get_app().config['MONITOR_LIMIT']).all():
                    Alchemy.db.session.delete(l)
                Alchemy.db.session.commit()

