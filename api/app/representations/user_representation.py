from representations.bin.representation import Representation


class UserRepresentation(Representation):

    def __init__(self, model):
        super().__init__(model)
