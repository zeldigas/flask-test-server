from representations.bin.representation import Representation
from representations.bin.link import Link
from representations.bin.action import Action, Field
from representations.bin.property import Property
from flask import url_for


class HookRepresentation(Representation):

    def __init__(self, model, box_id):
        super().__init__(box_id)
        if isinstance(model, (list, tuple)):
            create_hook_ac = Action.create(
                name='create-hook',
                href=url_for('hooks', _external=False, box_id=box_id),
                type='application/json',
                method='POST'
            )
            self.append(create_hook_ac)
            self.append([
                Link.create(
                    href=url_for('hooks', box_id=box_id, _external=False),
                    rel='self'
                ),
                Link.create(
                    href=url_for('box', box_id=box_id, _external=False),
                    rel='box'
                ),
                Property.create(
                    total=len(model)
                )
            ])
        elif model:
            self.append(
                [
                    Link.create(
                        href=url_for('hook', box_id=model.box_id, hook_id=model.id, _external=False),
                        rel='self'
                    ),
                    Link.create(
                        href=url_for('hooks', box_id=model.box_id, _external=False),
                        rel='hooks'
                    ),
                    Property.create(
                        created_at=model.created_at,
                        updated_at=model.updated_at,
                        content=model.content,
                        id=model.id,
                        request_headers=model.headers,
                        url_params=model.url_params
                    )
                ]
            )
