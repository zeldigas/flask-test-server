from datetime import datetime

from api.libs.utils.provider import Alchemy


class MonitorLog(Alchemy.db.Model):

    __tablename__ = 'monitor_logs'

    id = Alchemy.db.Column(Alchemy.db.Integer, primary_key=True)
    created_at = Alchemy.db.Column(Alchemy.db.TIMESTAMP, default=datetime.now, nullable=False)

    def __repr__(self):
        return '<MonitorLog id={1}, created_at={0}>'.format(self.created_at, self.id)