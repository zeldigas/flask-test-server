import hashlib
from datetime import datetime

from api.libs.utils.provider import Alchemy


class Hook(Alchemy.db.Model):

    __tablename__ = 'hooks'

    id = Alchemy.db.Column(Alchemy.db.Integer, primary_key=True)
    content = Alchemy.db.Column(Alchemy.db.Text, unique=False)
    created_at = Alchemy.db.Column(Alchemy.db.TIMESTAMP, default=datetime.now, nullable=False)
    updated_at = Alchemy.db.Column(
        Alchemy.db.TIMESTAMP, default=datetime.now, onupdate=datetime.now, nullable=False
    )
    box_id = Alchemy.db.Column(Alchemy.db.Integer, Alchemy.db.ForeignKey('boxes.id'))
    headers = Alchemy.db.Column(Alchemy.db.Text, nullable=False)
    url_params = Alchemy.db.Column(Alchemy.db.Text, nullable=False)

    def __init__(self, content=None, headers=None, url_params=None):
        self.content = content
        self.url_params = url_params
        self.headers = headers

    def __repr__(self):
        return '<Hook id={1}, content_hash={0} ...>'.format(
            hashlib.md5(self.content.encode('utf-8')).hexdigest(), self.id
        )
