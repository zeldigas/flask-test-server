from api.libs.utils.provider import Alchemy
# from passlib.apps import custom_app_context as pwd_context


class User(Alchemy.db.Model):

    __talblename__ = 'users'

    id = Alchemy.db.Column(Alchemy.db.Integer, primary_key=True)
    username = Alchemy.db.Column(Alchemy.db.Integer, unique=True)
    # password_hash = AlchemyDB.db.Column(AlchemyDB.db.String(128))

    def __init__(self, username=None):
        self.username = username
        # self.password_hash = ''

    def __repr__(self):
        return '<User id={1}, username={0}>'.format(self.username, self.id)
